<?php
namespace Drupal\practice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Link;
/**
 * Class DeleteForm.
 *
 * @package Drupal\mydata\Form
 */

class DeleteFormData extends ConfirmFormBase 
{
  /**
   * {@inheritdoc}
   */

  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) 
  {
    $query = \Drupal::database();
    $query->delete('practice_example')
                ->condition('id',$this->id)
               ->execute();
    \Drupal::messenger()->addMessage(t('Subscription has been deleted.'), 'success');
    $form_state->setRedirect('practice.show_records_controller_show');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "delete_form_data";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('practice.show_records_controller_show');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete %id?', ['%id' => $this->id]);
  }

}
