<?php

namespace Drupal\Practice\Form;
 
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormState;
use Drupal\Core\Link;
use Drupal\Core\Ajax\RedirectCommand;
use \Drupal\Core\Database\Connection;
use Drupal\Core\Mail;
use Drupal\Core\Mail\MailManager;

/**
 * Provides the form for adding countries.
 */

class PracticeForm extends FormBase 
{
 
  /**
   * {@inheritdoc}
   */

  public function getFormId() 
  {
    return 'PracticeForm';
  }
 
  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state) 
  {
    $get_path = explode("/", \Drupal::service('path.current')->getPath());
    $id = '';
    if($get_path[3] != '')
    {
      $id = $get_path[3];
      $result = \Drupal::database()->select('practice_example' , 'n')
              ->fields('n' , array('id', 'fname' , 'sname' , 'email' , 'age' , 'gender' , 'marks'))
              ->condition('id' , $get_path[3])
              ->execute()->fetchAll();
    /*  
        $var = "test";
        <?=$var?> => short hand operator : its basically used to print a single statement outside the php code. 
    */
      foreach($result as $key =>  $content)
      {
        $fname  = $content->fname;
        $sname  = $content->sname;
        $age    = $content->age;
        $email  = $content->email;
        $gender = $content->gender;
        $marks  = $content->marks;
      }
    }

    $form['fname'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('First Name'),
      // '#required'       => TRUE,
      '#maxlength'      => 20,
      '#default_value'  => isset($fname)?$fname:'',
    ];

    $form['sname'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Second Name'),
      // '#required'       => TRUE,
      '#maxlength'      => 20,
      '#default_value'  => isset($sname)?$sname:'',
    ];

    $form['age'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Age'),
      // '#required'       => TRUE,
      '#maxlength'      => 20,
      '#default_value'  => isset($age)?$age:'',
    ];

    $form['email'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('email'),
      // '#required'       => TRUE,
      
      '#default_value'  => isset($email)?$email:'',
    ];

    $form['gender'] = [
      '#type'           => 'radios',
      '#title'          => $this->t('gender'),
      '#default_value'  => isset($gender)?$gender:'',
      '#options' =>[
        'male'   =>'Male',
        'female' =>'Female',
        'other'  =>'Other',
      ],
      
    ];
    
    // is set => it will check that variable is set or not ?

    $form['marks'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Marks'),
      // '#required'       => TRUE,
      '#maxlength'      => 20,
      '#default_value'  => isset($marks)?$marks:'',
    ];
  
    $form['actions']['#type'] = 'actions';
    $form['actions']['Save'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $id != '' ? $this->t('Update') : $this->t('Save'),
    ];
   $form['actions']['clear'] = [
      '#type' => 'submit',
      // '#ajax' => ['callback' => '::clearForm','wrapper' => 'form-div',] ,
      '#value' => 'Clear',
     ];
    
    $form['actions']['clear'] = [
      '#type' => 'submit',
      // '#ajax' => ['callback' => '::clearForm','wrapper' => 'form-div',] ,
      '#value' => 'Clear',
     ];

     return $form;
 
  }

  //  Condition ? expression True : expression False
  //  1 == 1 ? 'True' : 'False'
  // Terronary Operator 
  
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) 
  {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) 
    {
      $response->addCommand(new ReplaceCommand('#practice_form', $form));
    }

    else 
    {
      $response->addCommand(new OpenModalDialogCommand("Success!",'The form has been submitted.',['width'=> 800]));
    }

    return $response;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) 
  {
    $get_path = explode("/", \Drupal::service('path.current')->getPath());
    if($get_path[3])
    {
      $fname            = $form_state->getValue('fname');
      $sname            = $form_state->getValue('sname');
      $age              = $form_state->getValue('age'); 
      $email            = $form_state->getValue('email');
      $gender           = $form_state->getValue('gender');
      $marks            = $form_state->getValue('marks');
      $result           = \Drupal::database()->update('practice_example')
          ->fields([
              'fname'   => $fname,
              'sname'   => $sname,
              'age'     => $age,
              'email'   => $email,
              'gender'  => $gender,
              'marks'   => $marks,
            ])

          ->condition('id' , $get_path[3])
          ->execute();
          
    }
    else
    {
      $fname            = $form_state->getValue('fname');
      $sname            = $form_state->getValue('sname');
      $age              = $form_state->getValue('age'); 
      $email            = $form_state->getValue('email');
      $gender           = $form_state->getValue('gender');
      $marks            = $form_state->getValue('marks');

      $result           = \Drupal::database()->insert('practice_example')
          ->fields([
              'fname'   => $fname,
              'sname'   => $sname,
              'age'     => $age,
              'email'   => $email,
              'gender'  => $gender,
              'marks'   => $marks,
            ])->execute();

        if($result > 0 || $result == true)
        {

          $msg .= 'Name : '.$fname.' '.$sname. 
          $msg .= 'Age : '.$age.
          $msg .= 'Email : '.$email.
          $msg .= 'Gender : '.$gender.
          $msg .= 'Marks : '.$marks.

          $mailManager       = \Drupal::service('plugin.manager.mail');
          $headers = [
                        'MIME-Version' => '1.0',
                        'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
                        'Content-Transfer-Encoding' => '8Bit',
                        'X-Mailer' => 'Drupal',
                    ];
          $module            = 'practice';
          $key               = 'general_mail';
          $to                = $email;
          $params['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed; delsp=yes';
          $params['message'] = $msg;
          $params['subject'] = "Mail subject";
          $langcode          = \Drupal::currentUser()->getPreferredLangcode();
          $send              = true;

          $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
        }

        if($result > 0 || $result == true)
        {
          \Drupal::messenger()->addMessage(t('Record has been added.'), 'success');
          $form_state->setRedirect('practice.show_records_controller_show');
        }
   
    }
  }

  
  public function validateForm(array &$form, FormStateInterface $form_state) 
  {
       
    if ( $form_state->getValue('fname') == "") 
    {
      $form_state->setErrorByName('from', $this->t('You must enter a valid first name..!'));
    }

    elseif( $form_state->getValue('marks') == "")
    {
       $form_state->setErrorByName('marks', $this->t('You must enter a valid to marks..!'));
    }

    elseif( $form_state->getValue('age') == "")
    {
      $form_state->setErrorByName('age' , $this->t('You must enter your age..!'));    
    }

    elseif( $form_state->getValue('email') == "")
    {
      $form_state->setErrorByName('email' , $this->t('You must enter the email..!'));
    }
  }
}
 


