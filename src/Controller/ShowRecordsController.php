<?php

namespace Drupal\practice\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Link;
use Drupal\Core\Asset\LibraryDiscoveryParser;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Component\Serialization\Json;

/**
 * Class ShowRecordsController.
 * 
 */

class ShowRecordsController extends ControllerBase 
{

    /**
     * Hello.
     *
     * @return string
     *   Return Hello string.
     */

  public function hello() 
  {
    

  }

    /**
     * Show.
     *
     * @return string
     *   Return Hello string.
     */

  public function PracticeData() 
  {
    
    $header     = [
      // 'id'      => t('ID'),
      'fname'   => t('Name'),
      'sname'   => t('Surname'),
      'age'     => t('Age'),
      'email'   => t('E-mail'),
      'gender'  => t('Gender'),
      'marks'   => t('Marks'),
    // 'view'   => t('View'),
      'edit'    => t('Edit'),
      'delete'  => t('Delete'),
    ];

    $result = \Drupal::database()->select('practice_example' , 'n')
              ->fields('n' , array('id', 'fname' , 'sname' , 'age' , 'email' , 'gender' , 'marks'))
              ->execute()->fetchAll();
    

    foreach($result as $key =>  $content)
    {
          
      $edit_link   = Url::fromRoute('practice.edit_records_controller_PracticeData',['id'=>$content->id],[]); 
      // $edit_link                ->setOptions([
      //   'attributes'            => [
      //     'class'                 => ['use-ajax', 'button', 'button--small'],
      //     'data-dialog-type'      => 'form',
      //     'data-dialog-options'   => Json::encode(['width' => 800]), 
      //   ]
      // ]);


      $delete_link = Url::fromRoute('practice.delete_records_controller_PracticeData',['id'=>$content->id],[]);
      // $delete_link              ->setOptions([
      //   'attributes'            => [
      //     'class'                 => ['use-ajax', 'button', 'button--small'],
      //     'data-dialog-type'      => 'form',
      //     'data-dialog-options'   => Json::encode(['width' => 800]),
      //   ]
      // ]);


      $row[$key]  = [
        'id'      => $content->id,
        'fname'   => $content->fname, 
        'sname'   => $content->sname, 
        'age'     => $content->age, 
        'email'   => $content->email,
        'gender'  => $content->gender, 
        'marks'   => $content->marks,
     // 'view'    => Link::fromTextAndUrl(t('View'), $view_link)->toString(),
        'edit'    => Link::fromTextAndUrl(t('Edit'), $edit_link)->toString(),
        'delete'  => Link::fromTextAndUrl(t('Delete'), $delete_link)->toString(),
      ];
    }

    $response = new AjaxResponse();
    $modal_form = \Drupal::formBuilder()->getForm('Drupal\practice\Form\PracticeForm');
    $response->addCommand(new OpenModalDialogCommand('Practice Form', $content, ['width' => '800']));

    
    $build        = [
      '#type'     => 'table',
      '#theme'    => 'practice',
      '#attached' => ['library' => ['practice/practice-styling.tree']],
      '#header'   => $header,
      '#rows'     => $row

    ];
    
    return $build;
  }


  public function edit($id)
  {
      $result = \Drupal::database()->update('practice_example')
                ->condition('id' , $content->id)
                ->execute();
                \Drupal::messenger()->addMessenger('Data Successfully Added...!');
                $formstate->setRedirect('practice.practice');
  }
  
}

